﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MamaMVVMProjekt.Model {
    public class Customer {
        private float cost;
        public string MeasureUnit { get; set; }
        public string ProductName { get; set; }
        public string DossierSymbol { get; set; }
        public string InvoiceNumber { get; set; }
        public float Cost {
            get { return cost; }
            set { cost = value; }
        }
        public string Destiny { get; set; }
        public float Income { get; set; }
        public float Expenditure { get; set; }
        
        
        public DateTime Date { get; set; }




    }
}
