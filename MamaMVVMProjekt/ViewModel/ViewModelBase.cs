﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MamaMVVMProjekt.ViewModel {
    public abstract class ViewModelBase {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged ( [CallerMemberName]string propertyName = null ) {
            if ( PropertyChanged != null )
                PropertyChanged (this, new PropertyChangedEventArgs (propertyName));
        }

        public void Dispose ( ) {
            OnDispose();
        }
            
        protected virtual void OnDispose ( ) { }
    }
}
