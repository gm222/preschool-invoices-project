﻿using GalaSoft.MvvmLight;
using MamaMVVMProjekt.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using System.Linq;

namespace MamaMVVMProjekt.ViewModel {
    public class NowaKartotekaViewModel : ViewModelBase {

        public System.Collections.ObjectModel.ObservableCollection<Customer> Customers { get; private set; }

        public NowaKartotekaViewModel ( ) {
            ObservableCollection<Customer> asd = new ObservableCollection<Model.Customer> ( );
            for ( int i = 0; i < 40; i++ ) {
                var customers = new Customer ( ) { Cost = 5.5f * ( i + 1 ), MeasureUnit = "kd", DossierSymbol = "awq", ProductName = "PODS", Income = ( i + 1 ) * 3, Date = DateTime.Now, Destiny = "asdasd", Expenditure = 2.1f * ( i + 1 ), InvoiceNumber = i.ToString ( ) + "a" };
                asd.Add (customers);
            }
            CurrentCustomer = asd.FirstOrDefault ( );
            Customers = asd;
        }

        private Customer _currentCustomer;

        public Customer CurrentCustomer {
            get { return _currentCustomer; }
            set {
                _currentCustomer = value;
                OnPropertyChanged ( );
            }
        }

        private bool CheckCurrentCustomer {
            get {
                return _currentCustomer != null;
            }
        }
        //? Tutaj trzeba zrobic lokalne properties
        //TODO: Trzeba zrobić by w XAML sam to sobie liczył
        public float Value {
            get {
                if ( CheckCurrentCustomer ) return CurrentCustomer.Income - CurrentCustomer.Expenditure;
                else throw new ArgumentNullException ("Current Customer Null Exception");
            }
        }

        public float Sum {
            get {
                if ( CheckCurrentCustomer ) return Value * CurrentCustomer.Cost;
                else throw new ArgumentNullException ("Current Customer Null Exception");
            }
        }

        public string InvoiceNumber { get { return "TEST"; } }
    }
}